import React, {Component} from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ScrollView>
        <View
          style={{
            marginHorizontal: 20,
            marginVertical: 10,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View>
            <View
              style={{
                backgroundColor: 'black',
                height: 2,
                width: 20,
                marginVertical: 2,
              }}></View>
            <View
              style={{
                backgroundColor: 'black',
                height: 2,
                width: 20,
              }}></View>
            <View
              style={{
                backgroundColor: 'black',
                height: 2,
                width: 20,
                marginVertical: 2,
              }}></View>
          </View>
          <Image
            source={require('../image/user.jpg')}
            style={{width: 50, height: 50}}
            borderRadius={50}
          />
        </View>
        <View>
          <Text
            style={{
              color: 'black',
              fontSize: 20,
              fontWeight: 'bold',
              marginLeft: 20,
            }}>
            Hello Gus Barok
          </Text>
          <Text
            style={{
              color: 'grey',
              fontSize: 10,
              fontWeight: 'bold',
              marginLeft: 20,
              marginVertical: 10,
            }}>
            Apa yang sedang anda rasakan sekarang?
          </Text>
        </View>
        <View
          style={{
            height: 200,
            flexDirection: 'column',
            justifyContent: 'space-around',
            borderWidth: 1,
            borderRadius: 20,
            borderColor: 'green',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 15,
            }}>
            <TouchableOpacity
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: 80,
                height: 80,
              }}>
              <View
                style={{
                  width: 80,
                  height: 80,
                  backgroundColor: 'red',
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  source={require('../image/about.png')}
                  style={{
                    width: 80,
                    height: 80,
                    backgroundColor: 'red',
                    borderRadius: 20,
                    width: 50,
                    height: 50,
                  }}></Image>
                <Text style={{color: 'white'}}>About</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 80,
                height: 80,
                backgroundColor: 'red',
                borderRadius: 20,
              }}>
              <View
                style={{
                  width: 80,
                  height: 80,
                  backgroundColor: 'red',
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  source={require('../image/cek.png')}
                  style={{
                    width: 80,
                    height: 80,
                    backgroundColor: 'red',
                    borderRadius: 20,
                    width: 50,
                    height: 50,
                  }}></Image>
                <Text style={{color: 'white'}}>Checkup</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 80,
                height: 80,
                backgroundColor: 'red',
                borderRadius: 20,
              }}>
              <View
                style={{
                  width: 80,
                  height: 80,
                  backgroundColor: 'red',
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  source={require('../image/hub.png')}
                  style={{
                    width: 80,
                    height: 80,
                    backgroundColor: 'red',
                    borderRadius: 20,
                    width: 50,
                    height: 50,
                  }}></Image>
                <Text style={{color: 'white'}}>Contact</Text>
              </View>
            </TouchableOpacity>
          </View>
          <Image
            source={require('../image/obat.png')}
            style={{
              width: 80,
              height: 80,
              backgroundColor: 'blue',
              borderRadius: 90,
              width: 50,
              height: 50,
            }}></Image>
          <Text style={{color: 'black'}}>obat</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 15,
            }}>
            <TouchableOpacity
              style={{
                width: 80,
                height: 80,
                backgroundColor: 'red',
                borderRadius: 20,
              }}></TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 80,
                height: 80,
                backgroundColor: 'red',
                borderRadius: 20,
              }}></TouchableOpacity>
            <TouchableOpacity
              style={{
                width: 80,
                height: 80,
                backgroundColor: 'red',
                borderRadius: 20,
              }}></TouchableOpacity>
          </View>
        </View>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <ScrollView
            horizontal={true}
            style={{
              width: 330,
              height: 180,
              marginVertical: 10,
            }}>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'green',
              }}></View>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'blue',
              }}></View>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'red',
              }}></View>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'yellow',
              }}></View>
          </ScrollView>
          <View
            style={{
              backgroundColor: 'pink',
              width: 330,
              height: 180,
              borderRadius: 20,
              marginBottom: 10,
            }}></View>
          <View
            style={{
              backgroundColor: 'purple',
              width: 330,
              height: 180,
              borderRadius: 20,
              marginRight: 10,
            }}></View>
        </View>
      </ScrollView>
    );
  }
}

export default Home;
