import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';

class Service extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ScrollView>
        <View>
          <Text
            style={{
              color: 'black',
              fontSize: 20,
              marginLeft: 10,
              marginTop: 10,
            }}>
            Service
          </Text>
          <View style={{alignItems: 'center', marginTop: 5}}>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'green',
                marginVertical: 10,
              }}></View>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'red',
                marginVertical: 10,
              }}></View>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'black',
                marginVertical: 10,
              }}></View>
            <View
              style={{
                width: 330,
                height: 180,
                borderRadius: 20,
                marginRight: 10,
                backgroundColor: 'purple',
                marginVertical: 10,
              }}></View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default Service;
